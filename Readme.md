# Sending Emails With Google Protobuf (Protocol Buffers)
### A simple server-client style to send emails from one end to another

---
You can hookup any GUI to this program & get yourself an Email Client.

####**What can it do:**
1. Define an Email *of course :)* & a name for the sender.
2. Define an Email & name for one or more recipients *(you can define one or more recipient)*.
3. Define one or more CC *Carbon Copy*
4. Define one or more BCC *Blind Carbon Copy*
5. Construct an email with subject and message.
6. Attach zero or more files *Optional*.


---

####**How to use:**

1. open terminal and run `python server`
This will start a localhost server on port **5050**.


2. open another terminal and run `python client` 
This is the client that will send an email to the server side.

3. Follow the instructions on the screen for the client then check the server terminal for the sent email *(All sent attachments are stored in a directory called **attachments**)*.


