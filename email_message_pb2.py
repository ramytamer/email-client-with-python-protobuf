# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: email_message.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='email_message.proto',
  package='lab2',
  syntax='proto2',
  serialized_pb=_b('\n\x13\x65mail_message.proto\x12\x04lab2\"\xd4\x02\n\x0c\x45mailMessage\x12)\n\x06sender\x18\x01 \x02(\x0b\x32\x19.lab2.EmailMessage.Person\x12,\n\trecipient\x18\x02 \x03(\x0b\x32\x19.lab2.EmailMessage.Person\x12%\n\x02\x63\x63\x18\x03 \x03(\x0b\x32\x19.lab2.EmailMessage.Person\x12&\n\x03\x62\x63\x63\x18\x04 \x03(\x0b\x32\x19.lab2.EmailMessage.Person\x12\x0f\n\x07subject\x18\x05 \x02(\t\x12\x0c\n\x04text\x18\x06 \x02(\t\x12,\n\x0b\x61ttachments\x18\x07 \x03(\x0b\x32\x17.lab2.EmailMessage.File\x1a\'\n\x06Person\x12\x0f\n\x07\x61\x64\x64ress\x18\x01 \x02(\t\x12\x0c\n\x04name\x18\x02 \x01(\t\x1a&\n\x04\x46ile\x12\x10\n\x08\x66ilename\x18\x01 \x02(\t\x12\x0c\n\x04\x64\x61ta\x18\x02 \x02(\x0c')
)
_sym_db.RegisterFileDescriptor(DESCRIPTOR)




_EMAILMESSAGE_PERSON = _descriptor.Descriptor(
  name='Person',
  full_name='lab2.EmailMessage.Person',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='address', full_name='lab2.EmailMessage.Person.address', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='name', full_name='lab2.EmailMessage.Person.name', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=291,
  serialized_end=330,
)

_EMAILMESSAGE_FILE = _descriptor.Descriptor(
  name='File',
  full_name='lab2.EmailMessage.File',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='filename', full_name='lab2.EmailMessage.File.filename', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='data', full_name='lab2.EmailMessage.File.data', index=1,
      number=2, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=332,
  serialized_end=370,
)

_EMAILMESSAGE = _descriptor.Descriptor(
  name='EmailMessage',
  full_name='lab2.EmailMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='sender', full_name='lab2.EmailMessage.sender', index=0,
      number=1, type=11, cpp_type=10, label=2,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='recipient', full_name='lab2.EmailMessage.recipient', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='cc', full_name='lab2.EmailMessage.cc', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='bcc', full_name='lab2.EmailMessage.bcc', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='subject', full_name='lab2.EmailMessage.subject', index=4,
      number=5, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='text', full_name='lab2.EmailMessage.text', index=5,
      number=6, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='attachments', full_name='lab2.EmailMessage.attachments', index=6,
      number=7, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[_EMAILMESSAGE_PERSON, _EMAILMESSAGE_FILE, ],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=30,
  serialized_end=370,
)

_EMAILMESSAGE_PERSON.containing_type = _EMAILMESSAGE
_EMAILMESSAGE_FILE.containing_type = _EMAILMESSAGE
_EMAILMESSAGE.fields_by_name['sender'].message_type = _EMAILMESSAGE_PERSON
_EMAILMESSAGE.fields_by_name['recipient'].message_type = _EMAILMESSAGE_PERSON
_EMAILMESSAGE.fields_by_name['cc'].message_type = _EMAILMESSAGE_PERSON
_EMAILMESSAGE.fields_by_name['bcc'].message_type = _EMAILMESSAGE_PERSON
_EMAILMESSAGE.fields_by_name['attachments'].message_type = _EMAILMESSAGE_FILE
DESCRIPTOR.message_types_by_name['EmailMessage'] = _EMAILMESSAGE

EmailMessage = _reflection.GeneratedProtocolMessageType('EmailMessage', (_message.Message,), dict(

  Person = _reflection.GeneratedProtocolMessageType('Person', (_message.Message,), dict(
    DESCRIPTOR = _EMAILMESSAGE_PERSON,
    __module__ = 'email_message_pb2'
    # @@protoc_insertion_point(class_scope:lab2.EmailMessage.Person)
    ))
  ,

  File = _reflection.GeneratedProtocolMessageType('File', (_message.Message,), dict(
    DESCRIPTOR = _EMAILMESSAGE_FILE,
    __module__ = 'email_message_pb2'
    # @@protoc_insertion_point(class_scope:lab2.EmailMessage.File)
    ))
  ,
  DESCRIPTOR = _EMAILMESSAGE,
  __module__ = 'email_message_pb2'
  # @@protoc_insertion_point(class_scope:lab2.EmailMessage)
  ))
_sym_db.RegisterMessage(EmailMessage)
_sym_db.RegisterMessage(EmailMessage.Person)
_sym_db.RegisterMessage(EmailMessage.File)


# @@protoc_insertion_point(module_scope)
