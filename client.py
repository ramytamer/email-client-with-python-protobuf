#! /usr/bin/python

import socket
import sys
import time
from random import randint
from threading import Thread
import os
import email_message_pb2 as email_message

tcp_client_host = 'localhost'
port = 5050

def start(h='localhost', p=5050, clients=1):
    global host
    host = h
    global port
    port = p

    global server_address
    server_address = (host, port)

    main(clients)


def new_client(client_id, repeat, delay):

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print "client -> Client #%d connecting to %s:%s" % (client_id, server_address[0], server_address[1])

    try:
        sock.connect((host, port))
        start = time.clock()
        # Send data
        i = 0
        while i < repeat:
            email = email_message.EmailMessage()
            
            email.sender.address = raw_input('Sender email address: ')
            name = raw_input('Sender name (leave blank for none): ')
            if name != "":
                email.sender.name = name
            
            to_person = email.recipient.add()
            to_person.address = raw_input('Recipient email address: ')
            name = raw_input('Recipient name (leave blank for none): ')
            if name != "":
                to_person.name = name
            
            while True:
                address = raw_input('Recipient email address (leave blank to finish): ')
                if address == '':
                    break
                to_person = email.recipient.add()
                to_person.address = address
                name = raw_input('Recipient name (leave blank for none): ')
                if name != "":
                    to_person.name = name

            while True:
                address = raw_input('Carbon Copy [CC] Email address (leave blank to finish): ')
                if address == '':
                    break
                cc_person = email.cc.add()
                cc_person.address = address
                name = raw_input('Carbon Copy [CC] name (leave blank for none): ')
                if name != "":
                    cc_person.name = name

            while True:
                address = raw_input('Blind Carbon Copy [BCC] Email address (leave blank to finish): ')
                if address == '':
                    break
                bcc_person = email.bcc.add()
                bcc_person.address = address
                name = raw_input('Blind Carbon Copy [BCC] name (leave blank for none): ')
                if name != "":
                    bcc_person.name = name

            email.subject = raw_input('Email subject: ')
            email.text = raw_input('Email message: ')

            while True:
                attachment_file_path = raw_input('Attachment File (leave blank to finish): ')
                if attachment_file_path == '':
                    break
                if os.path.exists(attachment_file_path):
                    print 'File exists' ,
                    filedata = ''
                    f = open(attachment_file_path, 'rb')
                    data = f.read(1024)
                    while data:
                        filedata += data
                        data = f.read(1024)
                    f.close()
                    attach = email.attachments.add()
                    attach.filename = os.path.basename(attachment_file_path)
                    attach.data = filedata
                    print ', File assigned Successfully.'
                    


            email_serialized = email.SerializeToString()
            
            # print 'sending: '
            # print email_serialized
            # print '=' * 20

            sock.sendall(email_serialized)

            # time.sleep(delay)
            
            amount_received = 0
            amount_expected = len("success")
            
            while amount_received < amount_expected:
                data = sock.recv(amount_expected)
                amount_received += len(data)
                print "client -> Client #%d Received: %s After %f seconds\n" % (client_id, data, (time.clock() - start))

            i += 1

    except Exception, e:
        print "client -> Closing Connection for client #%d." % (client_id)
        sock.close()
    finally:
        print "client -> Closing Connection #%d." % (client_id)
        sock.close()

def main(num_clients = 2):

    clients = num_clients

    i = 1
    threads = []
    while i <= clients:
        t = Thread(None, new_client, None, (i, 1, randint(1, 3)))
        t.start()
        threads.append(t)
        i += 1

    for t in threads:
        t.join()


clients = 1
if len(sys.argv) > 1:
    clients = int(sys.argv[1])


start(clients=clients)