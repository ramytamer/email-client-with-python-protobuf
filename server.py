import socket
import sys
import thread
import signal
import errno
import email_message_pb2 as email_message
import time

client_num = 1
EXIT_FLAG = False
host = ''
port = 5050

def close_server(signal, frame):
    exit_server()

signal.signal(signal.SIGINT, close_server)

def start(h='', p=5050):
    global host
    host = h
    global port
    port = p

    main()

def client_thread(client_socket, client_address, client_num):
    try:
        print "New connection from %s:%s " % (client_address[0], client_address[1])

        while True:
            data = client_socket.recv(500000000)

            if data:
                email = email_message.EmailMessage()
                email.ParseFromString(data)

                print '=' * 20
                print 'GOT NEW E-MAIL'
                print '-' * 20
                print 'From: %s <%s>' % (email.sender.name, email.sender.address)
                print 'To: ',
                for rec in email.recipient:
                    print '%s <%s>, ' % (rec.name, rec.address) ,
                print ''
                print 'CC: ',
                for rec in email.cc:
                    print '%s <%s>, ' % (rec.name, rec.address) ,
                print ''
                print 'BCC: ',
                for rec in email.bcc:
                    print '%s <%s>, ' % (rec.name, rec.address) ,
                print ''
                print '*' * 20
                print 'Subject: ', email.subject
                print 'Message: ', email.text

                print '*' * 20
                print len(email.attachments) , 'Attachments'
                print '=' * 20

                for attach in email.attachments:
                    filenamestr = "%d-%s" % (int(time.time()), attach.filename)
                    f = open('attachments/' + filenamestr, 'wb')
                    f.write(attach.data)
                    f.close()

                client_socket.sendall("success")
            else:
                break
    except Exception:
        pass
    finally:
        print "%s:%s closed" % (client_address[0], client_address[1])
        client_socket.close()



def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    print "Starting Server at %s:%s" % (host, port)
    sock.bind((host, port))

    sock.listen(5)


    while True:
        # print EXIT_FLAG
        if EXIT_FLAG:
            break

        print "Waiting for a connection"
        try:
            client_socket, client_address = sock.accept()

            global client_num
            thread.start_new_thread(client_thread, (client_socket, client_address, client_num))
            client_num += 1
        except socket.error, e:
            if e.errno != errno.EINTR:
                raise

       


    print "closed connection"
    sock.close()
        
   
def exit_server():
    # print '\b\b\b\b'
    global EXIT_FLAG
    EXIT_FLAG = True


start()